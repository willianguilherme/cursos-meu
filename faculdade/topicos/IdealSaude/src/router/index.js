import { createRouter, createWebHistory } from "vue-router";
import Imc from "../components/organisms/Imc.vue";
import Home from "../components/organisms/Home.vue";
import Login from "../components/organisms/Login.vue";
import CriarLogin from "../components/organisms/CriarLogin.vue";
import QuemSomos from "../components/organisms/QuemSomos.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/home",
      name: "home",
      component: Home,
    },
    {
      path: "/",
      name: "login",
      component: Login,
    },
    {
      path: "/criar-usuario",
      name: "criar-usuario",
      component: CriarLogin,
    },
    {
      path: "/imc",
      name: "imc",
      component: Imc,
    },
    {
      path: "/quem-somos",
      name: "quem-somos",
      component: QuemSomos,
    },
  ],
});

export default router;
