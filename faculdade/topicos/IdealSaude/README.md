## startando o projeto

Para iniciar a aplicação é necessário instalar as dependencias do projeto
Dentro da pasta do projeto rode 'npm i' ou 'npm install'
Após ter as dependencias instaladas precisamos rodar o projeto
No terminal rode 'npm start' para rodar a aplicação Vue.js

## informações

o login funciona a partir do localstorage, onde é possivel guardar um usuario por sessão, ao criar um user o anterior é apagado.
não é necessário logar para navegar pelas telas, o login é apenas para mostrar a utilização dos inputs, js's, etc.
a tela de criar login possui os demais inputs requeridos no trabalho.
na tela de home temos um carrosel de imagens, para utilizar clique e arraste as imagens.
